#include <iostream>
#include <stdio.h>
#include<locale.h>
#include "Header.h"
using namespace std;

int main()
{
	setlocale(LC_ALL, "Rus");
	int a, b, modp = 0;
	cout << "Введите первое число: ";
	cin >> a;
	cout << "Введите второе число: ";
	cin >> b;
	while (modp == 0) {
		cout << "Введите модуль: ";
		cin >> modp;
	}
	IntModulo(a, b, modp);
	return 0;
}

/*
  Создайте класс с именем IntModulo для выполнения действий с целыми
	по модулю P, указанному в конструкторе
*/ 